import SpriteKit

class GameScene: SKScene {
    
    // Varibles
    private var player: Player?
    private var mainCamera: SKCameraNode?
    
    // Get centef of the screen
    private var center: CGFloat?
    
    // Is game started
    private var isStarted: Bool = false
    
    // Variables - before update
    private var __playerPositionY: CGFloat?
    
    // Variables - Background
    private var background: SKNode?
    
    // Variables - Control style - Side
    private var canMoveBySide = false
    private var lastTouches = Set<UITouch>()
    
    // Variables - Control style - Swipe
    private var lastMoveLocationX: CGFloat?
    
    // Generate Objects
    private var gameObject: GameObjectNode?
    
    // DidMove
    override func didMove(to view: SKView) {
        initializeScene()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        canMoveBySide = true
        lastTouches = touches
        
        if !isStarted {
            startGame()
            isStarted = true
        }
        
        if Settings.shared.controlStyle == .Swipe {
            self.moveBySwipeBegan(touches, with: event)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if Settings.shared.controlStyle == .Swipe {
            self.moveBySwipeMove(touches, with: event)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        canMoveBySide = false
        
        if Settings.shared.controlStyle == .Swipe {
            self.moveBySwipeEnd(touches, with: event)
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        if Settings.shared.controlStyle == .TwoSideButton && canMoveBySide {
            self.moveByTwoSideButton(touches: lastTouches)
        }
        
        moveCamera()
        gameObject?.randomNodeGanerator()
        checkIsRemoval()
        
        player?.update()
    }
    
}

// MARK: INITIALIZE AND MOVE
extension GameScene {
    
    private func initializeScene() {
        // get center of the screen
        self.center = (self.scene?.size.width)! / (self.scene?.size.height)!
        
        // physics world
        physicsWorld.contactDelegate = self
        physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0)
        
        initializeGameObjectNode()
        initializePlayer()
        initializeCamera()
    }
    
    private func initializePlayer() {
        self.player = self.childNode(withName: "Player") as? Player
        self.player?.setup()
    }
    
    private func initializeCamera() {
        self.mainCamera = self.childNode(withName: "mainCamera") as? SKCameraNode
    }
    
    private func initializeGameObjectNode() {
        self.gameObject = GameObjectNode()
        self.gameObject?.initialize(inScene: self.scene!)
        
        self.background = self.gameObject?.createBackground()
        
        self.scene?.addChild(background!)
        //self.addChild(background!)
    }
    
    // Move Camera
    private func moveCamera() {
        if (self.mainCamera?.position.y)! < (self.player?.position.y)! {
            //moveBG()
            mainCamera?.position.y = (self.player?.position.y)!
        }
        
        let breakpoint = (self.mainCamera?.position.y)! - (self.size.height / 2) + ( 2 * (player?.size.height)!)
        if (self.player?.position.y)! < breakpoint && self.__playerPositionY != nil {
            precondition(self.__playerPositionY != nil)
            mainCamera?.position.y -= __playerPositionY! - (player?.position.y)!
        }
        
        self.__playerPositionY = self.player?.position.y
        
        moveBG()
    }
    
    private func moveBG() {
        self.background?.position.y = (mainCamera?.position.y)! * 0.95
    }
    
    
}

// MARK: CONTROL STYLE AND GAME START
extension GameScene {
    private func startGame() {
        player?.jump()
        self.physicsWorld.gravity = CGVector(dx: 0.0, dy: -10.0)
    }
    
    func moveByTwoSideButton(touches: Set<UITouch>) {
        for t in touches {
            let location = t.location(in: self)
            // move player
            if location.x > self.center! {
                player?.move(right: true, frame: self.frame)
            } else {
                player?.move(right: false, frame: self.frame)
            }
        }
    }
    
    func moveBySwipeBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            self.lastMoveLocationX = t.location(in: self).x
        }
    }
    
    func moveBySwipeMove(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            precondition(self.lastMoveLocationX != nil)
            
            let halfScreenWidth = self.size.width / 2
            let lastPointX = lastMoveLocationX! + halfScreenWidth
            let currentPointX = t.location(in: self).x + halfScreenWidth
            let moveStep = lastPointX - currentPointX
            
            player?.move(step: moveStep, frame: self.frame)
            
            lastMoveLocationX = t.location(in: self).x
            
        }
    }
    
    func moveBySwipeEnd(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.lastMoveLocationX = nil
    }
    
}


extension GameScene: SKPhysicsContactDelegate {
    func didBegin(_ contact: SKPhysicsContact) {
        // Player contact
        if contact.bodyB.node?.name == Player._name {
            let player = contact.bodyB.node as! Player
            // with collectable
            if let collectable = contact.bodyA.node as? ContatctPlayer {
                collectable.contactWithPlayer(player: player, gameObject: gameObject!)
            }
        }
    }
}

// CHECK IS REMOVEAL
extension GameScene {
    private func checkIsRemoval() {
        // remove platform
        self.gameObject?.nodePlatform.enumerateChildNodes(withName: Platform._name, using: { (node, _) in
            let platform = node as! Platform
            if platform.canRemove((self.player?.position.y)!, sceneHight: self.size.height) {
                self.gameObject?.queuePlatform.add(platform)
                platform.remove()
            }
        })
        
        // remove star
        self.gameObject?.nodeStar.enumerateChildNodes(withName: Star._name, using: { (node, _) in
            let star = node as! Star
            
            if star.canRemove((self.player?.position.y)!, sceneHight: self.size.height) {
                self.gameObject?.queueStar.add(star)
                star.remove()
            }
        })
        
        // remove boost
        self.gameObject?.nodeBoost.enumerateChildNodes(withName: Boost._name, using: { (node, _) in
            let boost = node as! Boost
            if boost.canRemove((self.player?.position.y)!, sceneHight: self.size.height) {
                self.gameObject?.queueBoost.add(boost)
                boost.remove()
            }
            
        })
    }
}








