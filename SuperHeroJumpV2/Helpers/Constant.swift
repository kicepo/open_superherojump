import Foundation
import SpriteKit


let REMOVED_GAME_SCENE_ELEMENT: CGPoint = CGPoint(x: 2000.0, y: 0.0)
let SPACING_BETWEEN_ELEMENTS: (MAX: CGFloat, MIN: CGFloat) = (MAX: 160.0, MIN: 80.0)

// Collisions category
struct CollisionCategoryBitMask {
    static let Player: UInt32 = 0x01
    static let Magnet: UInt32 = 0x1
    static let Platform: UInt32 = 0x10
    static let CollectableItem: UInt32 = 0x100
    
}
