import Foundation

struct Queue<T: GameElement> {
    
    var array: Array<T> = Array()
    
    init() {}
    
    mutating func get() -> T? {
        array.first?.isRemoved = false
        return array.removeFirst()
    }
    
    mutating func add(_ el: T) {
        el.isRemoved = true
        array.append(el)
    }
    
    func count() -> Int {
        return array.count
    }
}
