import UIKit
import SpriteKit

class Random {
    static func between( _ first: CGFloat, _ second: CGFloat ) -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UINT32_MAX) * abs(first - second) + min(first, second)
    }
    
    static var screenXCorrect: CGFloat {
        if UIDevice.current.iPhoneX {
            return 125
        } else { return 45 }
    }
    
}
