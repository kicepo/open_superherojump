import Foundation
import SpriteKit

extension CGVector: Comparable {
    public static func < (lhs: CGVector, rhs: CGVector) -> Bool {
        if lhs.dy < rhs.dy && lhs.dx < rhs.dx {
            return true
        }
        return false
    }
    
    
}
