import Foundation

enum ConrolStyle: Int {
    case Swipe = 1
    case TwoSideButton = 2
    case Gyroscope = 3
}

class Settings {
    static let shared: Settings = Settings();
    private init() {}
    
    public var controlStyle: ConrolStyle = .TwoSideButton
    
    public var currentHero: Int = 1
    public var currentLevel: Int = 1
    
}
