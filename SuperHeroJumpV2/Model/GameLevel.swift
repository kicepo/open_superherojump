import SpriteKit

struct GameLevel {
    var height = 500_000
    
    var multipler = (
        gravity: 1.0,
        jump: (
            stars:      1.0,
            platform:   1.0,
            boost:      1.0
        ),
        spaceBetween: (
            stars:      1.0,
            platform:   1.0,
            boost:      1.0
        )
    )
    
    var chanceFor = (
        star: 80,
        platform: 17,
        boost: 3
    )
    
    var overlay: GameOverlay = GameOverlay()
    
}
