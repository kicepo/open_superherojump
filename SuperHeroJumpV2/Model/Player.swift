import SpriteKit

class Player: SKSpriteNode {
    static let _name = "ObjectPlayer"
    
    // Player Animation
    private var currentPlayerAnimation: SKAction?
    
    // left, right, jump, fall
    private var animationJump: SKAction!
    private var animationFall: SKAction!
    private var animationSteerLeft: SKAction!
    private var animationSteerRight: SKAction!
    
    // effects
    private var normalJumpEffect: SKEmitterNode!
    
    public var timeBoost: Timer!
    
    public func setup() {
        self.name = Player._name
        self.zPosition = 10
        self.texture = SKTexture(imageNamed: "player01_fall_1")
        
        setupPhysic()
        self.normalJumpEffect = setupEffects()
        setupAnimations()
        
        
        timeBoost = Timer()
        
    }
    
}

// MARK: ============ PLAYER PHYSIC
extension Player {
    
    private func setupPhysic() {
        self.physicsBody = SKPhysicsBody(rectangleOf: self.size)
        self.physicsBody?.allowsRotation = false
        self.physicsBody?.affectedByGravity = true
        self.physicsBody?.isDynamic = true
        self.physicsBody?.usesPreciseCollisionDetection = true
        
        self.physicsBody?.restitution = 1.0
        self.physicsBody?.friction = 0.0
        self.physicsBody?.angularDamping = 0.0
        self.physicsBody?.linearDamping = 0.0
        
        self.physicsBody?.categoryBitMask = CollisionCategoryBitMask.Player
        self.physicsBody?.collisionBitMask = 0
        self.physicsBody?.contactTestBitMask = CollisionCategoryBitMask.Platform | CollisionCategoryBitMask.CollectableItem
    }
}

// MARK: ============ ANIMATION AND EFFECTS SETUP
extension Player {
    
    private func setupAnimations() {
        animationJump = createAnimations("player01_jump_", start: 1, end: 4, timePerFrame: 0.1)
        animationFall = createAnimations("player01_fall_", start: 1, end: 3, timePerFrame: 0.1)
        animationSteerLeft = createAnimations("player01_steerleft_", start: 1, end: 2, timePerFrame: 0.1)
        animationSteerRight = createAnimations("player01_steerright_", start: 1, end: 2, timePerFrame: 0.1)
    }
    
    private func createAnimations(_ prefix: String, start: Int, end: Int, timePerFrame: TimeInterval) -> SKAction {
        var textures = [SKTexture]()
        for i in start...end {
            textures.append(SKTexture(imageNamed: "\(prefix)\(i)"))
        }
        return SKAction.animate(with: textures, timePerFrame: timePerFrame)
    }
    
    private func runAnimation(_ animation: SKAction) {
        if currentPlayerAnimation == nil || currentPlayerAnimation! != animation {
            self.removeAction(forKey: "playerAnimation")
            self.run(animation, withKey: "playerAnimation")
            currentPlayerAnimation = animation
        }
    }
    
    private func addTrail(name: String) -> SKEmitterNode {
        let trail = SKEmitterNode(fileNamed: name)!
        trail.zPosition = -1
        trail.position.y -= self.size.height
        trail.xScale = 0.3
        trail.yScale = 0.3
        trail.particleBirthRate = 200
        self.addChild(trail)
        return trail
    }
    
    private func setupEffects() -> SKEmitterNode {
        let jumpEffect = SKEmitterNode(fileNamed: "JumpNormal")
        jumpEffect?.position = self.position
        jumpEffect?.zPosition = 6
        jumpEffect?.targetNode = self.scene
        jumpEffect?.name = "JumpEffect"

        return jumpEffect!
    }
    

}

// MARK: ============ PLAYER EFFECTS START/STOP
extension Player {
    private func removeEffects() {
        normalJumpEffect.removeFromParent()
    }
    
    private func addEffect() {
        if ((self.scene?.childNode(withName: "JumpEffect")) == nil) {
            self.scene?.addChild(normalJumpEffect)
        }
    }

}

// MARK: ============= PLAYER ACTIONS
extension Player {
    
    public func jump() {
        if (self.physicsBody?.velocity.dy)! > 1200 {
            return;
        }
        
        self.physicsBody?.velocity = CGVector(dx: (self.physicsBody?.velocity.dx)!, dy: 1200)
        runAnimation(animationJump)
    }
    
    public func jump(dy: CGFloat) {
        if (self.physicsBody?.velocity.dy)! > dy {
            return;
        }
        
        self.physicsBody?.velocity = CGVector(dx: (self.physicsBody?.velocity.dx)!, dy: dy)
        runAnimation(animationJump)
    
    }
    
    public func jumpByBoost(dy: CGFloat) {
        let gain: CGFloat = 2.5
        self.physicsBody?.velocity.dy = max((self.physicsBody?.velocity.dy)!, dy * gain)
        runAnimation(animationJump)
        
        // Animation effect
        addEffect()
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer) in
            self.removeEffects()
        })
    }
    
    public func move(right: Bool, frame: CGRect) {
        var move: CGFloat = 0.0
        
        if right {
            move = self.position.x + 10.0
            runAnimation(animationSteerRight)
        } else {
            move = self.position.x - 10.0
            runAnimation(animationSteerLeft)
        }
        
        self._move(move: move, frame: frame)
        
    }
    
    public func move(step: CGFloat, frame: CGRect) {
        let move: CGFloat = self.position.x - step
        
        self._move(move: move, frame: frame)
    }
    
    private func _move(move: CGFloat, frame: CGRect) {
        _ = self.size.width / 2
        
        if !((move > (frame.maxX)) || move < (frame.minX)) {
            self.position.x = move
        } else {
            let leftSide = self.position.x < 0
            
            if leftSide {
                self.position.x = frame.maxX - 13
                
            } else {
                self.position.x = frame.minX + 13
                
            }
        }
    }
}

// MARK: Update player
extension Player {
    func update() {
        guard let normal = self.normalJumpEffect else {return}
        normal.particlePosition = CGPoint(x: self.position.x, y: self.position.y + 250)
    }
}
