import SpriteKit

struct GameOverlay {
    
    var gradient = (
        startColor: CIColor(color: .white),
        endColor: CIColor(color: UIColor(displayP3Red: 115/255.0, green: 127/255.0, blue: 197/255.0, alpha: 1))
    )
    
    var background = (
        level_1: SKTexture(imageNamed: "bg1"),
        level_2: SKTexture(imageNamed: "bg2"),
        level_3: SKTexture(imageNamed: "")
    )
    
    var platform = (
        normal: SKTexture(imageNamed: "platform_normal1"),
        breakable: SKTexture(imageNamed: "platform_break1")
    )
    
    
}
