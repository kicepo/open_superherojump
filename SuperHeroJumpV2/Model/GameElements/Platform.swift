import SpriteKit

public enum PlatformType: Int {
    case Normal = 0
    case Break = 1
    
    static func random() -> PlatformType {
        let rand: Int = Int(arc4random_uniform(UInt32(_count)))
        return PlatformType(rawValue: rand)!
    }
    
    private static let _count: PlatformType.RawValue = {
        var maxValue: Int = 0
        while let _ = PlatformType(rawValue: maxValue) {
            maxValue += 1
        }
        return maxValue
    }()
}

class Platform: GameElement {
    static let _name = "PLATFORM_OBJECT"
    
    public var platformType: PlatformType = .Normal
    public var images: (normal: SKTexture, breakable: SKTexture)?
    
    public func setup(images: (normal: SKTexture, breakable: SKTexture)) {
        self.name = Platform._name
        self.zPosition = 9
        self.images = images
        randomType()
        setupPhysics()
    }
    
    private func randomType() {
        self.platformType = .random()
        
        switch self.platformType {
        case .Normal:
            self.texture = images?.normal
            break
        case .Break: // ZW 6 min
            self.texture = images?.breakable
            break
        }
        
        self.size = (self.texture?.size())!
        self.xScale = 0.7
        self.yScale = 0.7
    }
    
    private func setupPhysics() {
        self.physicsBody = SKPhysicsBody(texture: self.texture!, size: self.size)
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.isDynamic = false
        
        self.physicsBody?.categoryBitMask = CollisionCategoryBitMask.Platform
        self.physicsBody?.collisionBitMask = 0
    }
}

extension Platform: ContatctPlayer {
    func contactWithPlayer(player: Player, gameObject: GameObjectNode) {
        if (player.physicsBody?.velocity.dy)! <= CGFloat(0.0) {
            player.jump()
            DispatchQueue.main.async {
                if self.platformType == .Break {
                    self.remove()
                    gameObject.queuePlatform.add(self)
                }
            }
        }
    }
}

// Load effects
extension Platform {
    private func loadBrokenEffect() {
        let broken = SKEmitterNode(fileNamed: "BrokenPlatformEffect")
        broken?.zPosition = 1
        self.addChild(broken!)
    }
}
