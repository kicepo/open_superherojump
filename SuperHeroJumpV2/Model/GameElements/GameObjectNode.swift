import SpriteKit
import Foundation

class GameObjectNode {
    var FLAG = (
                initialized: false,
                isGeneratedNow: false)
    
    private var scene: SKScene?
    private var positionLastGeneratedItem: CGFloat = 0.0
    
    //
    public var level: GameLevel?
    
    // Nodes with object
    public var nodeStar = SKNode()
    public var nodePlatform = SKNode()
    public var nodeBoost = SKNode()
    
    // Queue
    public var queueStar: Queue<Star> = Queue()
    public var queuePlatform: Queue<Platform> = Queue()
    public var queueBoost: Queue<Boost> = Queue()
    
    // Pattern list
    private var patternList: NSDictionary?
    
    // Initialize
    public func initialize(inScene: SKScene) {
        self.scene = inScene
        loadLevel()
        loadPatterns()
        
        for _ in 0...70 {
            let star = Star()
            star.setup()
            star.remove() // Od poczatku bedzie traktowana jako usunieta
            queueStar.add(star)
            nodeStar.addChild(star)
        }
        
        for _ in 0...20 {
            let platform = Platform()
            platform.setup(images: (self.level?.overlay.platform)!)
            platform.remove()
            queuePlatform.add(platform)
            nodePlatform.addChild(platform)
        }

        for _ in 0...3 {
            let boost = JumpBoost()
            boost.setup()
            boost.remove()
            queueBoost.add(boost)
            nodeBoost.addChild(boost)
        }

        for _ in 0...2 {
            let boost = SizeDecrementBoost()
            boost.setup()
            boost.remove()
            queueBoost.add(boost)
            nodeBoost.addChild(boost)
        }
        
        for _ in 0...2 {
            let boost = SizeIncreaseBoost()
            boost.setup()
            boost.remove()
            queueBoost.add(boost)
            nodeBoost.addChild(boost)
        }
        // Dodawanie SKNode Star do glownej sceny
        inScene.addChild(nodeStar)
        inScene.addChild(nodePlatform)
        inScene.addChild(nodeBoost)
        
        // Oznaczanie informacji ze zostaly stworzone wszystkie elementy
        FLAG.initialized = true
    }
    
    private func canGenerate() -> Bool {
        
        // Nie pozwala na generowanie jezeli nie zostaly stworzone elementy
        if !FLAG.initialized {
            return false
        }
        
        // Nie pozwala na generowanie jezeli jakis element jest generowany
        if FLAG.isGeneratedNow {
            return false
        }
        
        // Nie pozwala jezeli queue Star zawiera mniej niz 14 elementow
        if queueStar.count() < 14 {
            return false
        }
        // ;----------; Platform < 4 el
        if queuePlatform.count() < 4 {
            return false
        }
        
        if queueBoost.count() < 1 {
            return false
        }
        
        return true
    }
    
    public func randomNodeGanerator() {
        if canGenerate() {
            FLAG.isGeneratedNow = true
            let randomNode = Int(arc4random_uniform(100))
            let changeFor = level?.chanceFor
            switch randomNode {
            case let x where x < (changeFor?.star)!:
                generateStar()
            case (changeFor?.star)!+1...(changeFor?.star)! + (changeFor?.platform)!:
                generatePlatform()
            default:
                generateBoost()
            }
            FLAG.isGeneratedNow = false
        }
    }
    
    private func randomStarPattern() -> Pattern? {
        let starPattern = patternList!["Star"] as! NSDictionary
        let random = arc4random_uniform(100)
        
        var randomPatternName = "Single"
        
        switch random {
        case 0...70:
            let random = arc4random_uniform(100)
            switch random {
            case 0...14:
                randomPatternName = "Trace"
            case 15...29:
                randomPatternName = "TripleSquare"
            case 30...57:
                randomPatternName = "Square"
            case 58...71:
                randomPatternName = "TripleSquare"
            case 72...89:
                randomPatternName = "UpSideDownTriangle"
            case 90...100:
                randomPatternName = "Trace"
            default:
                return nil
            }
        case 71...100:
            let random = arc4random_uniform(100)
            switch random {
            case 0...31:
                randomPatternName = "UpSideDownTriangle"
            case 32...67:
                randomPatternName = "Triangle"
            case 68...100:
                randomPatternName = "SingleTrace"
            default:
                return nil
            }
        default:
            return nil
        }
        
        let randomPattern = starPattern[randomPatternName] as! NSDictionary
        let pattern = Pattern(randomPattern)
        
        if pattern.getPointsCount() <= queueStar.count() {
            return pattern
        } else {return nil}
        
    }
    
    private func randomPlatformPattern() -> Pattern? {
        let platformPatter = patternList!["Platform"] as! NSDictionary
        
        let random = arc4random_uniform(100)
        
        switch random {
        case 0...30:
            let singlePattern = platformPatter["Single"] as! NSDictionary
            let pattern = Pattern(singlePattern)
            return pattern
        case 31...60:
            let doublePattern = platformPatter["Double"] as! NSDictionary
            let pattern = Pattern(doublePattern)
            return pattern
        case 61...100:
            let triple = platformPatter["Triple"] as! NSDictionary
            let pattern = Pattern(triple)
            return pattern
        default:
            return nil
        }
    }
    
    // Generate Star or Platform
    private func generateStar() {
        ///=========== GENEROWANIE ===============
        let pattern = randomStarPattern()
      
        if pattern != nil {
            let randomX = Random.between((self.scene?.frame.minX)!, (self.scene?.frame.maxX)! - (pattern?.size.width)!)
            let randomY = Random.between(SPACING_BETWEEN_ELEMENTS.MIN, SPACING_BETWEEN_ELEMENTS.MAX) + positionLastGeneratedItem
            
            for p in (pattern?.points)! {
                let star = queueStar.get()
                star?.position = CGPoint(x: randomX + p.position.x, y: CGFloat(randomY) + p.position.y)
            }
            positionLastGeneratedItem = randomY + (pattern?.size.height)!
        }
    }
    
    private func generatePlatform() {
        let pattern = randomPlatformPattern()
        if pattern != nil {
            let randomX = Random.between((self.scene?.frame.minX)!, (self.scene?.frame.maxX)! - (pattern?.size.width)!)
            let randomY = Random.between(SPACING_BETWEEN_ELEMENTS.MIN, SPACING_BETWEEN_ELEMENTS.MAX) + positionLastGeneratedItem
            for p in (pattern?.points)! {
                let platform = queuePlatform.get()
                platform?.position = CGPoint(x: randomX + p.position.x, y: CGFloat(randomY) + p.position.y)
            }
            positionLastGeneratedItem = randomY + (pattern?.size.height)!
        }
    }
    
    private func generateBoost() {
        let randomY = Random.between(SPACING_BETWEEN_ELEMENTS.MIN, SPACING_BETWEEN_ELEMENTS.MAX) + positionLastGeneratedItem
        
        let boost = queueBoost.get()
        boost?.position = CGPoint(x: 0.0, y: CGFloat(randomY))
        
        positionLastGeneratedItem = randomY + 50
    }
    
}

// Patterny
extension GameObjectNode {
    
    private func loadPatterns() {
        let patternBundle = Bundle.main.path(forResource: "Patterns", ofType: "plist")
        let patternData = NSDictionary(contentsOfFile: patternBundle!)
        patternList = patternData
    }
    
}

// Ladowanie poziomu
extension  GameObjectNode {

    func loadLevel() {
        self.level = GameLevel()
    }
    
}

// MARK: BACKGROUND
extension GameObjectNode {
    func updateBackground() {
        let startColor = level?.overlay.gradient.startColor
        let endColor = level?.overlay.gradient.endColor
        
        let texture = SKTexture(size: (self.scene?.size)!, color1: startColor!, color2: endColor!)
        texture.filteringMode = .nearest
        
        let sprite = SKSpriteNode(texture: texture)
        sprite.size = (self.scene?.size)!
        sprite.size.height = 100_000
        
        sprite.position = CGPoint(x: (self.scene?.frame.midX)!, y: sprite.size.height/2 - (scene?.size.height)!*2)
        
        self.scene?.addChild(sprite)
    }
    
    
    func createBackground() -> SKNode {
        
        let background = level?.overlay.background
        
        let node = SKNode()
        
        let bg1 = SKSpriteNode(texture: background?.level_1)
        bg1.zPosition = 3
        bg1.anchorPoint = CGPoint(x: 0.0, y: 0.0)
        bg1.position.y -= (self.scene?.size.height)! / 2
        bg1.position.x -= (self.scene?.size.width)! / 2
        
        let randomXPosition = Random.between(((self.scene?.size.width)!/2), bg1.size.width - ((self.scene?.size.width)! / 2))
        bg1.position.x = -randomXPosition
        node.addChild(bg1)
        
        let bg2 = SKSpriteNode(texture: background?.level_2)
        bg2.zPosition = 2
        bg2.anchorPoint = CGPoint(x: 0.0, y: 0.0)
        bg2.position.y -= (self.scene?.size.height)! / 2
        bg2.position.x -= (self.scene?.size.width)! / 2
        
        let randomXPosition2 = Random.between(((self.scene?.size.width)!/2), bg1.size.width - ((self.scene?.size.width)! / 2))
        bg2.position.x = -randomXPosition2
        node.addChild(bg2)
        node.zPosition = 5

        return node
    }
    
}









