import SpriteKit

class JumpBoost: Boost {
    override func contactWithPlayer(player: Player, gameObject: GameObjectNode) {
        DispatchQueue.main.async {
            player.jumpByBoost(dy: 900)
            self.remove()
            gameObject.queueBoost.add(self)
        }
    }
}
