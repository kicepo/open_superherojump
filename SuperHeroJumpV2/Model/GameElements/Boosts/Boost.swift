import SpriteKit

class Boost: GameElement, ContatctPlayer {
    func contactWithPlayer(player: Player, gameObject: GameObjectNode) {}
    
    static let _name = "BOOST_OBJECT"
    
    public func setup() {
        self.name = Boost._name
        self.zPosition = 9
        
        setupBoost()
        self.size = (self.texture?.size())!

        setupPhysics()
    }
    
    public func setupBoost() {
        self.texture = SKTexture(imageNamed: "boost_speedUp")

    }
    
    private func setupPhysics() {
        self.physicsBody = SKPhysicsBody(texture: self.texture!, size: self.size)
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.isDynamic = false
        
        self.physicsBody?.categoryBitMask = CollisionCategoryBitMask.CollectableItem
        self.physicsBody?.collisionBitMask = 0
    }
    
    private func fixTimer(player: Player) {
        if player.timeBoost != nil {
            player.timeBoost.invalidate()
        }
        
        player.timeBoost = nil
        player.timeBoost = Timer.scheduledTimer(withTimeInterval: 7, repeats: false) { (timer) in
            player.xScale = 1
            player.yScale = 1
        }
    }
    
    public func scaleBoostHandler(player: Player, toScale: CGFloat) {
        player.xScale = toScale
        player.yScale = toScale
        player.jump(dy: 1300)
        fixTimer(player: player)
    }
    
}

