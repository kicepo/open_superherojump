import SpriteKit

class SizeDecrementBoost: Boost {
    
    override func setupBoost() {
        self.texture = SKTexture(imageNamed: "SizeDecrement")
    }
    
    override func contactWithPlayer(player: Player, gameObject: GameObjectNode) {
        self.scaleBoostHandler(player: player, toScale: 0.5)
        DispatchQueue.main.async {
            self.remove()
            gameObject.queueBoost.add(self)
        }
    }
    
}
