import SpriteKit

class SizeIncreaseBoost: Boost {
    
    override func setupBoost() {
        self.texture = SKTexture(imageNamed: "SizeIncrease")
    }

    override func contactWithPlayer(player: Player, gameObject: GameObjectNode) {
        self.scaleBoostHandler(player: player, toScale: 2.5)
        DispatchQueue.main.async {
            self.remove()
            gameObject.queueBoost.add(self)
        }
    }
    
}
