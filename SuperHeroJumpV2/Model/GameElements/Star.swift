import SpriteKit

class Star: GameElement {
    
    static let _name = "STAR_OBJECT"
    
    public func setup() {
        
        self.name = Star._name
        self.zPosition = 9
        
        self.texture = SKTexture(imageNamed: "Star")
        self.size = (self.texture?.size())!
        
        setupPhysics()
    }
    
    private func setupPhysics() {
        self.physicsBody = SKPhysicsBody(texture: self.texture!, size: self.size)
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.isDynamic = false
        
        self.physicsBody?.categoryBitMask = CollisionCategoryBitMask.CollectableItem
        self.physicsBody?.collisionBitMask = 0
    }
}

extension Star: ContatctPlayer {
    func contactWithPlayer(player: Player, gameObject: GameObjectNode) {
        DispatchQueue.main.async {
            player.jump(dy: 1100)
            self.remove()
            gameObject.queueStar.add(self)
        }
    }
}

