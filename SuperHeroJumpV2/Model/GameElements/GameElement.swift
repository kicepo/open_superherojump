import Foundation
import SpriteKit

class GameElement: SKSpriteNode {
    
    var isRemoved: Bool = false
    
    func canRemove(_ playerPositionY: CGFloat, sceneHight: CGFloat) -> Bool {
        if !self.isRemoved && self.position.y + sceneHight < playerPositionY {
            return true
        }
        return false
    }
    
    func remove() {
        self.position = REMOVED_GAME_SCENE_ELEMENT
        self.isRemoved = true
    }
    
}
