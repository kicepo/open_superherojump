import Foundation
import SpriteKit

struct PatterElement {
    var position: CGPoint
    var type: Int
    
    init(point: CGPoint, type: Int) {
        self.position = point
        self.type = type
    }
}

struct Pattern {
    var size: CGSize
    var points = [PatterElement]()
    
    mutating func addElement(_ el: PatterElement) {
        points.append(el)
    }
    
    func getPoints() -> [PatterElement] {
        return points
    }
    
    func getPointsCount() -> Int {
        return self.points.count
    }
    
    init(_ dictionary: NSDictionary){
        let parse = dictionary as! Dictionary<String, AnyObject>
        let sizeParse = parse["Size"] as! Dictionary<String, CGFloat>
        let pointsParse = parse["Points"] as! [Dictionary<String, CGFloat>]
        
        self.size = CGSize(width: sizeParse["width"]!, height: sizeParse["height"]!)
        
        for point in pointsParse {
            let atPoint = CGPoint(x: point["x"]!, y: point["y"]!)
            let type = Int(point["type"]!)
            self.points.append(PatterElement(point: atPoint, type: type))
        }
        
        
    }
}
