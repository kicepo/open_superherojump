import UIKit

class MainMenuVC: UIViewController {
    @IBOutlet weak var play_btnOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playBtnSetup()
    }
}

// Btn setup
extension MainMenuVC {
    private func playBtnSetup() {
        play_btnOutlet.contentVerticalAlignment = .fill
        play_btnOutlet.contentMode = .center
    }
}
