import UIKit
import SpriteKit

class GameVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadScene()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        (self.view as? SKView)?.scene?.size = self.view.bounds.size
    }
    
    // ReceiveMemory Warning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func loadScene() {
        
        if #available(iOS 11.0, *), let view = self.view {
            view.frame = self.view.safeAreaLayoutGuide.layoutFrame
        }
        
        guard let view = self.view as! SKView? else {return}
        guard let scene = GameScene(fileNamed: "GameScene") else {return}
        
        scene.scaleMode = .aspectFill
        view.presentScene(scene)
        
        view.ignoresSiblingOrder = true
        view.showsFPS = true
        view.showsNodeCount = true
        view.showsPhysics = true
    }
}
