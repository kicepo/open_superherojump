import UIKit

class ChooseVC: UIViewController {
    
    @IBAction func EzLevel_btn(_ sender: Any) {
        self.chooseLevel_CollectionView.isHidden = false
    }
    @IBOutlet weak var chooseLevel_CollectionView: UICollectionView!
    private let cellResueId = "CellIdentifier"
    
    let testObject = ["Level 1"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        self.chooseLevel_CollectionView.register(UINib(nibName: "ChooseLevelCell", bundle: nil), forCellWithReuseIdentifier: cellResueId)
        self.chooseLevel_CollectionView.dataSource = self
        self.chooseLevel_CollectionView.delegate = self
    }
    
}

extension ChooseVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.testObject.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellResueId, for: indexPath) as! ChooseLevelCell
        cell.levelName.text = self.testObject[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "runLevel", sender: nil)
    }
}
